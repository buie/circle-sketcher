// nice green 52, 152, 219

// circle arrays take the form, x location, y location, diameter

int[][] circles = new int[10000][10000];
int overallArrayLocation = 0;

int windowWidth = 800;
int windowHeight = 800;

void setup(){
  size(windowWidth, windowHeight);
  //frameRate(30);
}

void draw() {
  background(52, 73, 94);
  fill(255);
  noStroke();
  ellipseMode(CENTER);
  for(int i = 0; i < circles.length; i++){
    int[] circle = circles[i];
    int x = circle[0];
    int y = circle[1];
    int d = circle[2];
    
    ellipse(x, y, d, d);
  }
  drawACircle();
}

void drawACircle() {
  boolean drawn = false;
  
  while(drawn == false){
  int randX = (int)random(800);
  int randY = (int)random(800);
  int randDiam = (int)random(10,160);
  
    for(int i = 0; i < circles.length; i++){
      int[] circle = circles[i];
      int x = circle[0];
      int y = circle[1];
      int d = circle[2];
      
      if((((x-randX)*(x-randX))+((y-randY)*(y-randY))) <= ((d/2+randDiam/2)*(d/2+randDiam/2))){  
        i = 0;
        randX = (int)random(800);
        randY = (int)random(800);
        randDiam = (int)random(10,160);
      }
    }
    addCircle(randX, randY, randDiam);
    drawn = true;
    
    //see if this circle will overlap an existing circle 
    //if it makes it to the end it should add the circle if it fucks up it should go back to the dop and retry
    

  }

}

void addCircle(int xloc, int yloc, int diameter){
  int[] circle = {xloc, yloc, diameter};
  circles[overallArrayLocation] = circle;
  overallArrayLocation++;
  
}
