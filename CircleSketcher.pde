/*
  CircleSketcher
  by Andrew Buie
*/

int[][] circles = new int[10000][3];
int overallArrayLocation = 0;

int windowWidth = 1200;
int windowHeight = 800;

int maxCircleDiameter = 100;
int minCircleDiameter = 2;

void setup(){
  size(windowWidth, windowHeight);
  frameRate(30);
}

void draw() {
  background(52, 73, 94);
  fill(255);
  noStroke();
  ellipseMode(CENTER);
  for(int i = 0; i < circles.length; i++){
    int x = circles[i][0];
    int y = circles[i][1];
    int d = circles[i][2];
    
    ellipse(x, y, d, d);
  }
  drawACircle();
}

void drawACircle() {
  boolean drawn = false;
  
  while(drawn == false){
  //generates a random x, y, and diameter
  int randX = (int)random(windowWidth);
  int randY = (int)random(800);
  int randDiam = (int)random(minCircleDiameter,maxCircleDiameter);
    
    /*
    iterate through the array of existing squares
    tests to see if our randomly generated circle
    overlaps any of the existing circle
    */
    for(int i = 0; i < circles.length; i++){
      //gets a circle
      int x = circles[i][0];
      int y = circles[i][1];
      int d = circles[i][2];
      
      if((((x-randX)*(x-randX))+((y-randY)*(y-randY))) <= ((d/2+randDiam/2)*(d/2+randDiam/2))){  
        i = 0;
        randX = (int)random(windowWidth);
        randY = (int)random(800);
        randDiam = (int)random(minCircleDiameter,maxCircleDiameter);
      }
    }
    addCircle(randX, randY, randDiam);
    drawn = true;
  }
}

void mouseClicked(){
  circles = new int[10000][3];
  overallArrayLocation = 0;
}

void addCircle(int xloc, int yloc, int diameter){
  circles[overallArrayLocation][0] = xloc;
  circles[overallArrayLocation][1] = yloc;
  circles[overallArrayLocation][2] = diameter;
  overallArrayLocation++;
}
